const UNDER_ATTACK = true;

class Backtracking {
    inBoardArea(coordinate) {
        if (coordinate >= 0 && coordinate < this.BOARD_SIZE) return true;
    }

    checkPosition(x, y) {
        // це для перевірки, що ми не вийшлі за рамки дошки
        return this.inBoardArea(x) && this.inBoardArea(y);
    }

    isFigureAttacks(row, j) { //перевіряємо коордінати що можуть бути під атакою
        for (let k = 0; k < this.board.length; k++) {
            for (let l = 0; l < this.board[0].length; l++) {
                const onBoard = this.checkPosition(k, l);
                // перевіряємо ходи королеви
                // атака по горизонталі
                if (onBoard && k === row && this.board[k][l] === 1) {
                    return UNDER_ATTACK;
                }
                // атака по вертікалі
                if (l === j && onBoard && this.board[k][l] === 1) {
                    return UNDER_ATTACK;
                }
                // атака по діагоналі
                if (Math.abs(row - k) === Math.abs(j - l) && onBoard && this.board[k][l] === 1) {
                    return UNDER_ATTACK;
                }
            }
        }

        // перевіряємо ходи коня
        const horsePositions = [
            [-2, -1],
            [-2, +1],
            [-1, +2],
            [+1, +2],
            [+2, +1],
            [+2, -1],
            [+1, -2],
            [-1, -2]
        ];

        for (let i = 0; i < horsePositions.length; i++) {
            const onBoard = this.checkPosition(row + horsePositions[i][0], j + horsePositions[i][1], this.board.length);
            const posX = parseInt(row)  + parseInt(horsePositions[i][0]);
            const posY = parseInt(j)  + parseInt(horsePositions[i][1]);

            if (this.board[posX] && this.board[posX][posY]) {
                if (onBoard && this.board[posX][posY] === 1) {
                    return UNDER_ATTACK;
                }
            }
        }

        return false // не атакована
    }

    findSolution(elementPosition) {
        if (elementPosition === this.entities.length) {
            // кількість нерозставлених фігур дорівнює 0, то друкуємо дошку
            this.printBoard();
            // та закінчуємо роботу
            return true;
        } else if (elementPosition < this.entities.length) {
            // або проходімо по дошці та дивимося, чи фігура може бути атакована
                for (let i = 0; i < this.board.length; i++) {
                    for (let j = 0; j < this.board[0].length; j++) {
                        // дивимося, чи фігура може бути атакована
                        if(!this.isFigureAttacks(i, j)) {
                            // ящо атакована, ставимо фігуру в клітинку
                            this.board[i][j] = 1;
                            // дивимося, чи атакована наступна фігура
                            if (this.findSolution(elementPosition + 1)) {
                                return true;

                            } else {
                                // якщо може бути атакована - прибираємо фігуру з клітінки
                                this.board[i][j] = 0;
                            }
                        }
                    }
                }
        }
        return false;
    }

    printBoard() {
        let board = '';
        for (let i = 0; i < this.BOARD_SIZE; i++) {
            board += this.board[i].toString() + '\n';
        }
        console.log(board);
    }

    main() {
        this.BOARD_SIZE = 10;
        const FIGURE_NUMBER = 9;
        // розстановка не спрацює якщо кількість фігур >= розміру дошки

        this.entities = [];

        for (let i = 0; i<FIGURE_NUMBER; i++ ){
            this.entities.push('M');
        }

        console.log('this.entities  ', this.entities);

        this.board = Array.from({length: this.BOARD_SIZE}, (v, k) => [...Array.from({length: this.BOARD_SIZE}, (v, k) => 0)]);
        this.findSolution(0);
    }
}

const backtracking = new Backtracking();
backtracking.main();
