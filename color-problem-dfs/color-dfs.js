const colors = ['red', 'blue', 'green']; // Доступні кольори

// кольори на початок
const nodes = {
    A: 'red',
    B: 'blue',
    C: 'blue',
    D: 'red',
    E: 'blue',
    F: 'blue',
}

class Graph {
    constructor(noOfVertices) {
        this.noOfVertices = noOfVertices;
        this.AdjList = new Map();
    }

    addVertex(v) {
        this.AdjList.set(v, []);
    }

    printGraph() {
        const get_keys = this.AdjList.keys();
        for (let i of get_keys) {
            const get_values = this.AdjList.get(i);
            let conc = "";

            for (let j of get_values)
                conc += j + " ";

            console.log(i + " -> " + conc);
        }
    }

    addEdge(v, w) {
        this.AdjList.get(v).push(w);
        this.AdjList.get(w).push(v);
    }

    dfs(startingNode) {
        let visited = {};
        this.DFSUtil(startingNode, visited);
    }

//  Обходимо рекурсивно ноди
    DFSUtil(vert, visited) {
        visited[vert] = true;
        console.log(vert);

        let get_neighbours = this.AdjList.get(vert);

        for (let i in get_neighbours) {
            let get_elem = get_neighbours[i];

            if (!this.constraintViolations(vert, get_elem)) {
                // якшо поряд 2 однакових кольори - пробуємо змінить колір
                this.changeColors(vert, get_elem);
            }

            console.log('Розраховані кольори: ', nodes)
            if (!visited[get_elem])
                this.DFSUtil(get_elem, visited);
        }
    }

    changeColors(vert, get_elem) {
        let goodColor = false;
            colors.forEach(color => {
                console.log('here', goodColor)
                // перебіраємо кольори
                nodes[get_elem] = color;
                // перевіряємо чи є 2 однакових
                goodColor = this.constraintViolations(vert, get_elem)
            })
    }

    constraintViolations(nodeA, nodeB) {
        // колір 1 не дорівнює кольору 2 - то повертаємо false
        return nodes[nodeA] !== nodes[nodeB];
    }
}

// Свворюємо граф
const g = new Graph(6);
const vertices = ['A', 'B', 'C', 'D', 'E', 'F'];

// додаємо вершини
for (var i = 0; i < vertices.length; i++) {
    g.addVertex(vertices[i]);
}

// додаємо ребра
g.addEdge('A', 'B');
g.addEdge('A', 'D');
g.addEdge('A', 'E');
g.addEdge('B', 'C');
g.addEdge('D', 'E');
g.addEdge('E', 'F');
g.addEdge('E', 'C');
g.addEdge('C', 'F');

// друкуємо граф
g.printGraph();
// запускаємо dfs
g.dfs('A');



